//
//  NotificationManager.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 04/12/2020.
//

import Foundation
import NotificationBannerSwift
import RxSwift
import RxCocoa

class NotificationManager {
    
    static let shared = NotificationManager()
    private let disposeBag = DisposeBag()
    private let bookNotices = PublishRelay<Notification>()
    
    private init() {
        setupShowNotices()
    }
    
    func appendNotification(type: BannerStyle, title: String) {
        bookNotices.accept(Notification(type: type, message: title))
    }
    
    func setupShowNotices() {
        bookNotices.asObservable()
            .subscribe(onNext: { notification in
                let banner = NotificationBanner(title: notification.message, subtitle: nil, style: notification.type)
                banner.show()
            })
            .disposed(by: self.disposeBag)
    }
}
