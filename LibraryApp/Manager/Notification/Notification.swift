//
//  Notification.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 13/12/2020.
//

import Foundation
import NotificationBannerSwift

struct Notification {
    var type: BannerStyle
    var message: String
}
