//
//  BookManagerRest.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 01/12/2020.
//

import Foundation
import Alamofire
import RxSwift
import RxCocoa

class BookManager {
    
    private var url: String
    var nonFiltetedBooks: [BookDomain] = []
    
    init(url: String) {
        self.url = url
    }
    
    func getBooks() -> Observable<[BookDomain]> {
        return Observable<[BookDomain]>.create { observer in
            AF.request(self.url, method: .get)
                .validate(statusCode: 200..<300)
                .responseData(completionHandler : { response in
                    switch response.result {
                    case .success( let data):
                        do {
                            let books = try JSONDecoder().decode([BookDomain].self, from: data)
                            self.nonFiltetedBooks = books
                            observer.onNext(books)
                            observer.onCompleted()
                        } catch {
                            print(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                })
            return Disposables.create()
        }
    }
    
    func deleteBook(id: Int) -> Observable<Int> {
        let deleteUrl = url + "/\(id)"
        return Observable<Int>.create { observer in
            AF.request(deleteUrl,
                       method: .delete)
                .validate(statusCode: 200..<300)
                .response(completionHandler : { (response) in
                    switch response.result {
                    case .success(_):
                        do {
                            observer.onNext(200)
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                })
            return Disposables.create()
        }
    }
    
    func saveBook(_ book: BookDomain) -> Observable<Int> {
        return Observable<Int>.create { observer in
            AF.request(self.url,
                       method: .post,
                       parameters: book,
                       encoder: JSONParameterEncoder.default)
                .validate(statusCode: 200...201)
                .response(completionHandler : { (response) in
                    switch response.result {
                    case .success(_):
                        do {
                            observer.onNext(201)
                            observer.onCompleted()
                        }
                    case .failure( _):
                        observer.onNext(500)
                        observer.onCompleted()
                    }
                })
            return Disposables.create()
        }
    }
    
    func editBook(_ book: BookDomain, id: Int) -> Observable<Int> {
        let putUrl = url + "/\(id)"
        return Observable<Int>.create { observer in
            AF.request(putUrl,
                       method: .put,
                       parameters: book,
                       encoder: JSONParameterEncoder.default)
                .validate(statusCode: 200..<300)
                .response(completionHandler : { (response) in
                    switch response.result {
                    case .success(_):
                        do {
                            observer.onNext(200)
                            observer.onCompleted()
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                })
            return Disposables.create()
        }
    }
}
