//
//  SwipeKeyboard.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 25/11/2020.
//

import UIKit

class SwipeKeyboard : UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var pickerData: [Int] {
        get {
            fatalError("Implement me!")
        }
    }
    
    var pickerView: UIPickerView {
        get {
            fatalError("Implement me!")
        }
    }
    
    var textFieldWithSwipe: UITextField! {
        get {
            fatalError("Implement me!")
        }
    }
    
    var accessoryToolbar: UIToolbar {
        get {
            let toolbarFrame = CGRect(x: 0, y: 0,
                                      width: view.frame.width, height: 44)
            let accessoryToolbar = UIToolbar(frame: toolbarFrame)
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done,
                                             target: self,
                                             action: #selector(onDoneButtonTapped(sender:)))
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil,
                                                action: nil)
            accessoryToolbar.items = [flexibleSpace, doneButton]
            accessoryToolbar.barTintColor = UIColor.white
            return accessoryToolbar
        }
    }
}

extension SwipeKeyboard: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView,
                    titleForRow row: Int,
                    forComponent component: Int) -> String? {
        return String(pickerData[row])
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int) {
        textFieldWithSwipe.text = String(pickerData[row])
    }
}

extension SwipeKeyboard : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    @objc func onDoneButtonTapped(sender: UIBarButtonItem) {
        if textFieldWithSwipe.isFirstResponder {
            textFieldWithSwipe.resignFirstResponder()
        }
    }
}
