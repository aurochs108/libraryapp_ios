//
//  PickerKeyboardVars.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 15/12/2020.
//

import UIKit

class PickerKeyboardForNumbers: SwipeKeyboard {

    override var pickerData: [Int] {
        get {
            var pickerViewData = [Int]()
            let actualYear = Calendar.current.component(.year, from: Date())
            
            for year in 1900...actualYear {
                pickerViewData.append(year)
            }
            return pickerViewData
        }
    }
    
    override var pickerView: UIPickerView {
      get {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.white
        return pickerView
      }
    }
    
    
}
