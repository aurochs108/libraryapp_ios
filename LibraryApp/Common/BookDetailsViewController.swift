//
//  BookDetailsViewController.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 12/12/2020.
//

import UIKit

class BookDetailsViewController {
    
    func setupBookDetails(book: BookDomain,
                          title: UITextField,
                          author: UITextField,
                          numberOfPages: UITextField,
                          yearOfPublication: UITextField) {
        title.text = book.title
        author.text = book.author
        numberOfPages.text = String(book.numberOfPages)
        yearOfPublication.text = String(book.yearOfPublication)
    }
    
    func changeTextFieldsEnability(with value: Bool,
                                  title: UITextField,
                                  author: UITextField,
                                  numberOfPages: UITextField,
                                  yearOfPublication: UITextField) {
        title.isEnabled = value
        author.isEnabled = value
        numberOfPages.isEnabled = value
        yearOfPublication.isEnabled = value
    }
}
