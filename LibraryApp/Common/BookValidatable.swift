//
//  BookValidator.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 02/12/2020.
//

import UIKit

protocol BookValidatable {
    
}
  
extension BookValidatable {
    
    func isTextFieldValueEmpty(textField: UITextField) -> Bool {
        if (textField.text == "") {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.layer.borderWidth = 1.0
            return true
        }
        textField.layer.borderColor = UIColor.black.cgColor
        return false
    }
    
    
    func isNumberOfPagesValid(_ textField: UITextField) -> Int? {
        
        if isNumberPositive(for: textField.text!) {
            textField.layer.borderColor = UIColor.black.cgColor
            let numberOfPages = Int(textField.text!)!
            return numberOfPages
        } else {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.layer.borderWidth = 1.0
            return nil
        }
    }
    
    func isNumberPositive(for value: String) -> Bool {
        if let pages = Int(value) {
            return pages > 0
        }
        return false
    }
}

