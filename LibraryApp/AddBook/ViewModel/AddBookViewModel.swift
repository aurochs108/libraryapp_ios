//
//  AddBookViewModel.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 13/12/2020.
//

import Foundation
import RxSwift

class AddBookViewModel: NSObject {
    
    let addBookApiResult: PublishSubject<Int> = PublishSubject()
    private let disposeBag = DisposeBag()
    private let bookManager = BookManager(url: "http://localhost:8080/books")
    
    override init() {
        super.init()
    }

    func saveBook(_ book: BookDomain) {
        bookManager.saveBook(book).subscribe { (responseCode) in
            self.addBookApiResult.onNext(responseCode)
        } onError: { (Error) in
            self.addBookApiResult.onError(Error)
        }.disposed(by: disposeBag)
    }
    
}
