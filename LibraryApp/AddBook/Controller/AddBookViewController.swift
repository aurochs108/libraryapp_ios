//
//  AddBookViewController.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 22/11/2020.
//

import UIKit
import RxSwift

class AddBookViewController: PickerKeyboardForNumbers, BookValidatable {
    
    private let addBookViewModel = AddBookViewModel()
    
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldAuthor: UITextField!
    @IBOutlet weak var labelNumberOfPages: UILabel!
    @IBOutlet weak var textFieldNumberOfPages: UITextField!
    @IBOutlet weak var textFieldYearOfPublication: UITextField!
    
    private var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupYearOfPublication()
        setupBookApiResultOutput()
    }
    
    private func setupYearOfPublication() {
        textFieldYearOfPublication.inputView = pickerView
        textFieldYearOfPublication.inputAccessoryView = accessoryToolbar
    }
    
    override var textFieldWithSwipe: UITextField! {
        get {
            return textFieldYearOfPublication
        }
    }
    
    func setupBookApiResultOutput() {
        addBookViewModel.addBookApiResult.subscribe {  event in
            switch event {
            case .next (let code):
                if (code < 300) {
                    NotificationManager.shared.appendNotification(type: .success, title: "Book added")
                    self.dismiss(animated: true, completion: nil)
                    return
                }
                NotificationManager.shared.appendNotification(type: .danger, title: "Cannot save book")
            case .error (let error):
                NotificationManager.shared.appendNotification(type: .danger, title: error.localizedDescription)
            case .completed: break
            }
        }.disposed(by: disposeBag)

    }
    
    @IBAction func addBookButtonPressed(_ sender: UIButton) {
        let isTitleEmpty = isTextFieldValueEmpty(textField: textFieldTitle)
        let isAuthorEmpty = isTextFieldValueEmpty(textField: textFieldAuthor)
        let isYearOfPublicationEmpty = isTextFieldValueEmpty(textField: textFieldYearOfPublication)
        
        let numberOfPages: Int
        
        if let pages = isNumberOfPagesValid(textFieldNumberOfPages) {
            numberOfPages = pages
        } else {
            return
        }
        
        if (!isTitleEmpty && !isAuthorEmpty && !isYearOfPublicationEmpty) {
            let title = textFieldTitle.text!
            let author = textFieldAuthor.text!
            let yearOfPublication = Int(textFieldYearOfPublication.text!)!
            
            let book = BookDomain(title: title, author: author,
                                  numberOfPages: numberOfPages, yearOfPublication: yearOfPublication)
            addBookViewModel.saveBook(book)
        }
    }

    @IBAction func libraryButtonPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
