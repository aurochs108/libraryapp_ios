//
//  Book.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 21/11/2020.
//

import Foundation
import RealmSwift

class Book: Object {
    
    @objc dynamic var title: String = ""
    @objc dynamic var author: String = ""
    @objc dynamic var numberOfPages: Int = 0
    @objc dynamic var yearOfPublication: Int = 0000
    
}
