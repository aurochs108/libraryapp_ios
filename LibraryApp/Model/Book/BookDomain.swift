//
//  BookDomain.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 01/12/2020.
//

import Foundation

struct BookDomain: Codable {
    
    var id: Int?
    var title: String
    var author: String
    var numberOfPages: Int
    var yearOfPublication: Int
}
