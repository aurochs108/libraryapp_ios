//
//  BookInfoViewController.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 22/11/2020.
//
import UIKit
import RxSwift

class BookInfoViewController: PickerKeyboardForNumbers, BookValidatable {
    
    var bookInfoViewModel: BookInfoViewModel = BookInfoViewModel()
    private let bookDetailsViewController = BookDetailsViewController()
    private let disposeBag = DisposeBag()
    
    @IBOutlet private var textFieldTitle: UITextField!
    @IBOutlet private var textFieldAuthor: UITextField!
    @IBOutlet private var textFieldNumberOfPages: UITextField!
    @IBOutlet private var buttonSave: UIButton!
    @IBOutlet private var buttonEditBook: UIButton!
    @IBOutlet private var textFieldYearOfPublication: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBookDetails()
        setupTextFieldsEnability(with: false)
        
        buttonSave.isHidden = true
        
        setupYearOfPublication()
        setupEditBookCallback()
        setupDeleteBookCallback()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    func setupTextFieldsEnability(with isEnabled: Bool) {
        bookDetailsViewController.changeTextFieldsEnability(with: isEnabled,
                                                            title: textFieldTitle,
                                                            author: textFieldAuthor,
                                                            numberOfPages: textFieldNumberOfPages,
                                                            yearOfPublication: textFieldYearOfPublication)
    }
    
    func setupBookDetails() {
        bookDetailsViewController.setupBookDetails(book: bookInfoViewModel.selectedBook,
                                                   title: textFieldTitle,
                                                   author: textFieldAuthor,
                                                   numberOfPages: textFieldNumberOfPages,
                                                   yearOfPublication: textFieldYearOfPublication)
    }
    
    func setupEditBookCallback() {
        bookInfoViewModel.editBookResultCode.subscribe { (responseCode) in
            NotificationManager.shared.appendNotification(type: .success, title: "Book eddited")
        } onError: { (Error) in
            NotificationManager.shared.appendNotification(type: .danger, title: "Cannot edit book")
        }.disposed(by: disposeBag)
    }
    
    func setupDeleteBookCallback() {
        bookInfoViewModel.deleteBookResultCode
            .subscribe { (responseCode) in
            NotificationManager.shared.appendNotification(type: .success, title: "Book deleted")
            self.navigationController?.popViewController(animated: true)
        } onError: { (Error) in
            NotificationManager.shared.appendNotification(type: .danger, title: "Cannot delete book")
        }.disposed(by: disposeBag)
    }
    
    private func setupYearOfPublication() {
        textFieldYearOfPublication.inputView = pickerView
        textFieldYearOfPublication.inputAccessoryView = accessoryToolbar
        textFieldYearOfPublication.text = String(bookInfoViewModel.selectedBook!.yearOfPublication)
    }
    
    override var textFieldWithSwipe: UITextField! {
        get {
            return textFieldYearOfPublication
        }
    }
    
    @IBAction func editBookPressed(_ sender: UIButton) {
        setupTextFieldsEnability(with: true)
        buttonSave.isHidden = false
        buttonEditBook.isHidden = true
    }
    
    @IBAction func deleteBookPressed(_ sender: Any) {
        bookInfoViewModel.deleteBook()
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        let numberOfPages: Int
        
        if let pages = isNumberOfPagesValid(textFieldNumberOfPages) {
            numberOfPages = pages
        } else {
            return
        }
        
        let titleEmpty = isTextFieldValueEmpty(textField: textFieldTitle)
        let authorEmpty = isTextFieldValueEmpty(textField: textFieldAuthor)
        let yearOfPublicationEmpty = isTextFieldValueEmpty(textField: textFieldYearOfPublication)
        
        if (!titleEmpty && !authorEmpty && !yearOfPublicationEmpty) {
            let title = textFieldTitle.text!
            let author = textFieldAuthor.text!
            let yearOfPublication = Int(textFieldYearOfPublication.text!)!
            
            let modifiedBook = BookDomain(title: title,
                                          author: author,
                                          numberOfPages: numberOfPages,
                                          yearOfPublication: yearOfPublication)
            
            bookInfoViewModel.editBook(modifiedBook)

            buttonSave.isHidden = true
            buttonEditBook.isHidden = false
            setupTextFieldsEnability(with: false)
        }
    }
}
