//
//  BookInfoViewModel.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 12/12/2020.
//

import Foundation
import RxSwift

class BookInfoViewModel : NSObject {
    
    var selectedBook: BookDomain!
    var editBookResultCode: PublishSubject<Int> = PublishSubject()
    var deleteBookResultCode: PublishSubject<Int> = PublishSubject()
    private let disposeBag = DisposeBag()
    private let bookManager = BookManager(url: "http://localhost:8080/books")
    
    override init() {
        super.init()
    }
    
    func deleteBook() {
        bookManager.deleteBook(id: selectedBook.id!).subscribe { (responseCode) in
            self.deleteBookResultCode.onNext(responseCode)
        } onError: { (Error) in
            self.deleteBookResultCode.onNext(500)
        }.disposed(by: disposeBag)
    }
    
    func editBook(_ book: BookDomain) {
        bookManager.editBook(book, id: selectedBook.id!).subscribe { (responseCode) in
            self.editBookResultCode.onNext(responseCode)
        } onError: { (Error) in
            self.editBookResultCode.onError(Error)
        }.disposed(by: disposeBag)
    }
}
