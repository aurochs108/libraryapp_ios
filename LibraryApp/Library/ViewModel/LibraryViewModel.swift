//
//  LibraryViewModel.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 11/12/2020.
//

import Foundation
import RxSwift
import RxCocoa

class LibraryViewModel : NSObject {
    
    private let bookManager = BookManager(url: "http://localhost:8080/books")
    var outputSubject: PublishSubject<[BookDomain]> = PublishSubject()
    var nonFilteredBookData: [BookDomain] = []
    var disposeBag = DisposeBag()
    
    
    override init() {
        super.init()
    }
    
    func fetchBooks() {
        bookManager.getBooks()
            .subscribe{ [weak self] event in
            guard let strongSelf = self else {return}
            switch event {
            case .next (let result):
                print(result)
                strongSelf.nonFilteredBookData = result
                strongSelf.outputSubject.onNext(result)
            case .error (let error):
                print(error)
                NotificationManager.shared.appendNotification(type: .danger, title: "Cannot fetch books")
            case .completed: break
            }
        }.disposed(by: disposeBag)
    }

    func filterBookResults(for signs: String) {
        if (signs != "") {
            let filttered = nonFilteredBookData.filter{ $0.title.hasPrefix(signs) }
            outputSubject.onNext(filttered)
        } else {
            outputSubject.onNext(nonFilteredBookData)
        }
    }
    
}
