//
//  LibraryViewController.swift
//  LibraryApp
//
//  Created by Dawid Żubrowski on 21/11/2020.
//

import UIKit
import RxSwift
import RxCocoa

class LibraryViewController: UIViewController, UITabBarControllerDelegate {
    
    @IBOutlet private var searchBar: UISearchBar!
    @IBOutlet private var bookTableView: UITableView!
    
    private var firstAppear = true
    private var disposeBag = DisposeBag()
    private let libraryViewModel = LibraryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        
        //callViewModelToUpdateUI()
        setupCellTapHandling()
        setupSearchBar()
        setupCellConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        if (firstAppear) {
//            firstAppear = false
//            return
//        }
        callViewModelToUpdateUI()
    }
    
    func callViewModelToUpdateUI() {
        libraryViewModel.fetchBooks()
    }
    
    func setupCellConfiguration() {
        libraryViewModel.outputSubject
            .bind(to: bookTableView.rx.items(cellIdentifier: "BookCell", cellType: UITableViewCell.self)) {
                (row, element, cell) in
                cell.textLabel?.text = element.title
            }
            .disposed(by: disposeBag)
    }
    
    func setupSearchBar() {
        searchBar
            .rx
            .text.orEmpty
            .asObservable()
            .subscribe (onNext: { text in
                self.libraryViewModel.filterBookResults(for: text)
            })
            .disposed(by: disposeBag)
    }
    
    func setupCellTapHandling() {
        bookTableView
            .rx
            .modelSelected(BookDomain.self)
            .subscribe(onNext: { [unowned self] book in
                if let selectedRowIndexPath = self.bookTableView.indexPathForSelectedRow {
                    self.bookTableView.deselectRow(at: selectedRowIndexPath, animated: true)
                }
                performSegue(withIdentifier: "goToBookInfo", sender: book)
            })
            .disposed(by: disposeBag)
    }
    
    @IBAction func addBookPressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "goToAddBook", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let sequeIdentifier = segue.identifier
        if (sequeIdentifier == "goToBookInfo") {
            guard let selectedBook = sender as? BookDomain else { return }
            guard let bookInfoVC = segue.destination as? BookInfoViewController else {
                fatalError("Can't prepare for segue goToBookInfo") }
            
            bookInfoVC.bookInfoViewModel.selectedBook = selectedBook
        }
    }
}
